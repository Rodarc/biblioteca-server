from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from datetime import date

class Author(models.Model):
    first_name = models.CharField(verbose_name=_('fist_name'), max_length = 50, blank = False)
    last_name = models.CharField(verbose_name=_('last_name'), max_length = 50, blank = False)
    country = models.CharField(verbose_name=_('country'), max_length = 50, blank = True)
    date_created = models.DateTimeField(default=timezone.now, verbose_name=_('date_created'))
    date_birth = models.DateField(default=date.today, verbose_name=_('date_birth'), blank = False)

    def __str__ (self):
        return self.first_name + " " + self.last_name

    class Meta:
        verbose_name = _('Author')
        verbose_name_plural = _('Authors')

def NameCoverImage(instance, filename):
    return '/'.join(['covers', filename])

class Book(models.Model):
    title = models.CharField(verbose_name=_('tile'), max_length = 100, blank = False)
    #description = models.CharField(verbose_name=_('description'), max_length=500, blank = True)
    cover = models.ImageField(upload_to=NameCoverImage, max_length=254, blank=True, null=True)
    date_created = models.DateTimeField(default=timezone.now, verbose_name=_('date_created'))
    date_publication = models.DateField(default=date.today, verbose_name=_('date_publication'), blank = False)
    edition = models.IntegerField(verbose_name=_('edition'), default = 1)
    number_copies = models.IntegerField(verbose_name=_('number_copies'), default = 0)
    author = models.ForeignKey(Author, verbose_name=_('author'), on_delete=models.CASCADE)

    def __str__ (self):
        return self.title

    class Meta:
        verbose_name = _('Book')
        verbose_name_plural = _('Books')