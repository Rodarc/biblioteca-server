from django.conf.urls import url, include
from .views import * 
from rest_framework.routers import DefaultRouter

app_name = 'library'

router = DefaultRouter()

router.register(r'author', AuthorViewSet, base_name='author')
router.register(r'book', BookViewSet, base_name='book')

urlpatterns = router.urls