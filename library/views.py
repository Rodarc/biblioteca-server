from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin
from rest_framework import status
from rest_framework.decorators import * 
from .models import Author, Book
from .serializers import AuthorSerializer, BookSerializer

class AuthorViewSet(ModelViewSet):
    serializer_class = AuthorSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Author.objects.all()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        author = serializer.save()
        author.save()
        return Response(self.get_serializer(author).data, status=status.HTTP_201_CREATED)


class BookViewSet(ModelViewSet):
    serializer_class = BookSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Book.objects.all()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        book = serializer.save()
        book.save()
        return Response(self.get_serializer(book).data, status=status.HTTP_201_CREATED)
    
    @action(detail=False)
    def search(self, request):
        phrase = self.request.query_params.get('phrase', None)
        if phrase is not None:
            qbook = Book.objects.filter(title__icontains=phrase)
            qauthorfn = Author.objects.filter(first_name__icontains=phrase)
            for author in qauthorfn:
                if(qbook.count()):
                    qbook.union(author.book_set.all())
                else:
                    qbook = author.book_set.all()
            qauthorln = Author.objects.filter(last_name__icontains=phrase)
            for author in qauthorln:
                if(qbook.count()):
                    qbook.union(author.book_set.all())
                else:
                    qbook = author.book_set.all()
            qbookjson = self.get_serializer(qbook, many=True)
            return Response(qbookjson.data)

    @action(detail=False)
    def filter_query(self, request):
        initialyear = self.request.query_params.get('initialyear', None)
        finalyear = self.request.query_params.get('finalyear', None)
        #tambien necesitare la lista de utores
        authors = self.request.query_params.getlist('authors', None) #para obtener array con los ids de actores filtrados

        qs = Book.objects.all()
        if initialyear is not None and finalyear is not None:
            if len(authors) > 0:
                qs = Book.objects.filter(date_publication__year__range=(initialyear, finalyear), author__in=authors).order_by('author', 'date_publication', 'title')
            else:
                qs = Book.objects.filter(date_publication__year__range=(initialyear, finalyear)).order_by('date_publication', 'author', 'title')
        else:
            if len(authors) > 0:
                qs = Book.objects.filter(author__in=authors).order_by('author', 'date_publication', 'title')

        phrase = self.request.query_params.get('phrase', None)
        if phrase is not None:
            qs = qs.filter(title__icontains=phrase)

        qbookjson = self.get_serializer(qs, many=True)
        return Response(qbookjson.data)