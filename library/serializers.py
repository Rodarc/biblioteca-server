from rest_framework import serializers
from .models import *

class AuthorSerializer (serializers.ModelSerializer):
    """
    Serializer for retrive task data
    """
    #deberia tener uno aparte para retornar con la data de los autores al que pertenece un libro?
    class Meta:
        model = Author 
        fields = ('id', 'first_name', 'last_name', 'country', 'date_birth')

class BookSerializer (serializers.ModelSerializer):
    """
    Serializer for retrive task data
    """
    #deberia tener uno aparte para retornar con la data de los autores al que pertenece un libro?
    class Meta:
        model = Book 
        fields = ('id', 'title', 'date_publication', 'cover', 'edition', 'number_copies', 'author')