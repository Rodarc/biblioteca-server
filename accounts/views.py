from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.viewsets import ModelViewSet, GenericViewSet, ViewSet, mixins
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin
from rest_framework import status

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout 
from .serializers import UserSerializer, UserRegisterSerializer


class RegisterUserViewSet(GenericViewSet, CreateModelMixin):
    """
    View set that includes the user register view
    """
    serializer_class = UserRegisterSerializer
    permission_classes = (AllowAny,)

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        user.save()
        return Response(self.get_serializer(user).data, status=status.HTTP_201_CREATED)


class LoginUserViewSet(GenericViewSet, CreateModelMixin):
    """
    ViewSet that includes the user login view
    """

    serializer_class = AuthTokenSerializer 
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        """
        Validate the user and returns a token

        :param request: request information
        :return: Response with the user token
        :rtype Response
        """
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        print(token.key)
        return Response({'token': token.key})

    def post_create(self):
        pass

class LogoutUserViewSet(ModelViewSet):
    serializer_class = UserSerializer 
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        self.request.user.auth_token.delete()
        qs = User.objects.filter(id = self.request.user.pk)
        return qs


class ProfileUserViewSet(GenericViewSet, RetrieveModelMixin):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()

    def retrieve(self, request, *args, **kwargs):
        if(request.user.pk == int(kwargs['pk'])):#esto no se debe hacer aqui, quiza con una clase de permiso personalizada
            queryset = User.objects.all()
            user = User.objects.get(id=kwargs['pk'])
            serializer = self.serializer_class(user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

