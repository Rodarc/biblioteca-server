from rest_framework import serializers
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for retrive user data
    """
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'last_login')


class UserRegisterSerializer(serializers.ModelSerializer):
    """
    Serializer for create a new user
    """
    
    def create(self, validate_data):
        print("creteing user")
        print(validate_data['username'])
        print(validate_data['password'])
        user = User.objects.create_user(**validate_data)
        return user
    

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password') #este incluye la contraseña
        extra_kwargs = {'password': {'write_only': True}}