from django.conf.urls import url, include
from .views import * 
from rest_framework.routers import DefaultRouter

app_name = 'accounts'

router = DefaultRouter()

router.register(r'register', RegisterUserViewSet, base_name='register')
router.register(r'login', LoginUserViewSet, base_name='login')
router.register(r'logout', LogoutUserViewSet, base_name='logout')
router.register(r'profile', ProfileUserViewSet, base_name='profile')

urlpatterns = router.urls
