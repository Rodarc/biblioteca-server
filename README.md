# Biblioteca

## Requerimientos

### Base de datos
PostgreSQL

### Requirimientos Entorno virtual
```
Django==2.2.4
django-cors-headers==3.0.2
django-environ==0.4.5
django-filter==2.2.0
djangorestframework==3.10.2
Markdown==3.1.1
Pillow==6.1.0
psycopg2==2.8.3
pytz==2019.2
sqlparse==0.3.0
```

## Preparación
Crear una base de datos en PostgreSQL para el proyecto.

Crear un entorno virtual para el proyecto:
```
virtualenv biblioteca-server-env
```

En la carpeta del entorno crear un archivo .env con la siguiente informacion
```
DEBUG=on
DATABASE_URL=psql://[user]:[password]@localhost:5432/[databasename]
ALLOWED_HOSTS=*
```

Clonar el respositorio.

Ingresar a la carpeta del repositorio, instalar los requerimientos del proyecto:
```
pip install -r requirements.txt
```

Realizar las migraciones.
```
python manage.py migrate
```


## Ejecucion
Iniciar el servidor de prueba de django en el puerto 8000
```
python manage.py runserver 
```

## URLs
El servidor trabaja con autorizacin por token:

### Login
Method: Post

Body:
- username
- password
```
http://127.0.0.1:8000/accounts/login/
```

### Register user 
Method: Post

Body:
- username
- password
- first_name
- last_name
- email
```
http://127.0.0.1:8000/accounts/register/
```

### Logout
Method: Get

Headers:
- Authorization: Token "token"
```
http://127.0.0.1:8000/accounts/logout/
```

### Lista de todos los libros
Method: Get

Headers:
- Authorization: Token "token"
```
http://127.0.0.1:8000/library/book/
```

### Detalle de un libro
Method: Get

Headers:
- Authorization: Token "token"
```
http://127.0.0.1:8000/library/book/<id>/
```

### Crear libro 
Method: Post

Headers:
- Authorization: Token "token"

Body:
- title
- cover 
- date_publication
- edition
- number_copies 
- author
```
http://127.0.0.1:8000/library/book/
```

### Actualizacion parcial de libro 
Method: Patch

Headers:
- Authorization: Token "token"

Body:
- title
- cover 
- date_publication
- edition
- number_copies 
- author
```
http://127.0.0.1:8000/library/book/<id>/
```

### Eliminacion de libro 
Method: Delete 

Headers:
- Authorization: Token "token"
```
http://127.0.0.1:8000/library/book/<id>/
```

### Busuqeda de libros por autor y titulo
Method: Get

Headers:
- Authorization: Token "token"

Params:
- phrase
```
http://127.0.0.1:8000/library/book/search/?phrase=<terminos>
```

### Filtrado de libros por autor y año de publicacion
Method: Get

Headers:
- Authorization: Token "token"

Params:
- authors (array)
- initialyear
- finaalyear
```
http://127.0.0.1:8000/library/book/filter_query/?authors=<autor 1>&authors=<autor 2>&...&initialyear=<año inicial>&finalyear=<año final>
```

### Lista de todos los autores
Method: Get

Headers:
- Authorization: Token "token"
```
http://127.0.0.1:8000/library/author/
```

### Detalle de un autor
Method: Get

Headers:
- Authorization: Token "token"
```
http://127.0.0.1:8000/library/author/<id>/
```

### Crear libro 
Method: Post

Headers:
- Authorization: Token "token"

Body:
- first_name
- last_name
- country
- date_birth
```
http://127.0.0.1:8000/library/author/
```

